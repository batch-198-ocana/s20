function showMessage(){
    console.log("Good morning starshine!");
}

//while loop
let count = 5;

while(count !== 0){
    console.log(count);
    count--;

}

//do while loops = can do task at least once even if condition is false
do{
    console.log(count);
    count--;
} while(count === 0);

// mini activity
let counterMini = 20;

while(counterMini > 0){
    console.log(counterMini);
    counterMini--;
}

//for loop
for(let count = 0; count <= 20; count++){
    console.log(count);
}

//mini activity (debugging)
for(let x = 1; x < 10; x++){
    let sum = x + 1;
    console.log("The sum of " +"1" +" + " +x +" = " +sum);
}

//continue and break
for(let counter = 0; counter <=20; counter++){
    if(counter % 2 === 0){
        continue;
    }
    // continue keyword disregards the following code block and the next loop is executed
    console.log(counter)
    
    if(counter > 10){
        break;
    }
}

for(let i = 1; i <=100; i++){
    if(i % 5 === 0){
        console.log(i);
    }
}