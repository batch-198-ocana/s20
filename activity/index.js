let number = prompt("Give me a number:");
console.log("The number you provided is " +number);


for(let i = parseInt(number); i >= 0; i--){
    if(i === 50){
        console.log("The current value is at 50. Terminating the loop.");
        break;
    }

    if(i % 10 === 0){
        console.log("The number is divisible by 10. Skipping the number.");
    } else if(i % 5 === 0){
        console.log(i);
    }
}

function multiplicationTable(){
    for(let num = 1; num <= 10; num++){
        let product = 5 * num;
        console.log("5 x " +num +" = " +product);
    }
}

multiplicationTable();